/**
 * This poackage contains webservice utility classes.
 *  - Generic servlet class that can be overridden to create custom servlets
 *    used to process incoming http requests.
 *  - Service factory class used to generate the retrofitted endpoint service
 *
 * @since 1.0
 * @see com.tocchae
 */
package com.tocchae.commons.webservice.webservice;
