/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.webservice;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.tocchae.commons.webservice.entities.WebserviceTransaction;
import com.tocchae.commons.webservice.interfaces.IResponse;
import com.tocchae.commons.webservice.response.DefaultResponseBody;
import com.tocchae.commons.webservice.response.DefaultResponseBodyDetail;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * This class contains the reusable components for each web endpoint
 *
 * @author Torti Ama-Njoku
 * @param <T> the broker object linked to this integration
 */
@SuppressWarnings("serial")
public abstract class AbstractWebservice<T extends WebserviceTransaction> extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public abstract void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException;

    /**
     * Handles the HTTP <code>GET</code> method. creates the service factory
     * used by the gateways to convert the code to soap
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(AbstractWebservice.class.getName()).log(Level.FATAL, ex.getMessage(), ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(AbstractWebservice.class.getName()).log(Level.FATAL, ex.getMessage(), ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    /**
     * Builds a proper JSON formatted response
     *
     * @param success boolean (if the request was successful)
     * @param detail response to be put into the detail part of the json
     * response
     * @return JSON formatted
     * @deprecated this method should not be used instead use the method
     * {@link #buildResponse(WebserviceTransaction) }
     * to build a default response or if the response is not default overide
     * this method in your servlet class
     */
    @Deprecated
    public String getJSONResponse(boolean success, Map<String, String> detail) {
        try {
            ByteArrayOutputStream internalOut = new ByteArrayOutputStream();
            JsonFactory jfactory = new JsonFactory();

            JsonGenerator generator = jfactory.createJsonGenerator(internalOut);
            generator.writeStartObject(); //{
            generator.writeBooleanField("success", success);
            generator.writeStartObject("detail"); //{

            // Iterate and add the detail section
            for (Map.Entry<String, String> entry : detail.entrySet()) {
                generator.writeStringField(entry.getKey(), entry.getValue());
            }

            generator.writeEndObject(); //}
            generator.writeEndObject(); //}
            generator.close();

            return internalOut.toString("UTF-8");

        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(AbstractWebservice.class
                    .getName()).log(Level.FATAL, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AbstractWebservice.class
                    .getName()).log(Level.FATAL, null, ex);
        }
        return "";
    }

    /**
     * This Method builds the request String from the HttpServletRequest
     *
     * @param request the request coming into the endpoint
     * @return A String representation of the request
     */
    public String getRequestString(HttpServletRequest request) {
        StringBuilder stringBuild = new StringBuilder();
        try {
            Map<String, String[]> parameters = request.getParameterMap();
            for (Map.Entry<String, String[]> entry : parameters.entrySet()) {
                String key = entry.getKey();
                String[] value = entry.getValue();
                stringBuild.append(key)
                        .append(" = ")
                        .append(value[0])
                        .append("\n");
            }
            stringBuild.append(IOUtils.toString(request.getInputStream()));
        } catch (IOException ex) {
            Logger.getLogger(AbstractWebservice.class.getName()).log(Level.FATAL, null, ex);
            stringBuild = new StringBuilder("IO Exception");
        }
        return stringBuild.toString();
    }

    /**
     * Checks the HttpServletRequest to see if all the request parameters were
     * provided
     *
     * @param request HttpServletRequest
     * @param paramsToCheck List of parameters that should be evaluated
     * @return ParamCheck
     */
    protected AbstractWebservice.ParamCheck checkRequestParameters(HttpServletRequest request,
                                                                   List<String> paramsToCheck) {
        for (String param : paramsToCheck) {
            if (request.getParameter(param) == null
                    || request.getParameter(param).isEmpty()) {
                return new ParamCheck(false, "Missing " + param);
            }
        }
        // Everything is OK, 
        return new ParamCheck(true, "");
    }

    /**
     * Internal private class used for checking the parameter status
     */
    protected final class ParamCheck {

        private boolean status;
        private String response;

        /**
         * Constructor
         *
         * @param status success/failure
         * @param response message to display
         */
        ParamCheck(boolean status, String response) {
            this.status = status;
            this.response = response;
        }

        /**
         * @return the status
         */
        public boolean isSuccessful() {
            return status;
        }

        /**
         * @return the response
         */
        public String getResponse() {
            return response;
        }

    }

    /**
     * This method setup the inheriting webservice so that if can be prossessed
     * this includes creating the broker that will be used during the use case duration
     *
     * @param httpServletRequest this is the stream from where we get incoming
     * web services
     * @param response this is the portal by which we will return a response
     * @param webserviceTransaction the instance of the broker transaction
     * @throws IOException if unable to get the writer for the response throw it
     * hard
     * @deprecated this method is no longer used and will be removed in an
     * upcoming release
     */
    @Deprecated
    public void setUpWebservice(HttpServletRequest httpServletRequest, HttpServletResponse response, T webserviceTransaction)
            throws IOException {
    }

    /**
     * this method will build the sting response that the servlet returns to the
     * client
     *
     * @param transaction a broker object that contains all the details of the
     * response
     * @return the string response to be returned
     */
    public String getWebserviceResponse(T transaction) {
        //build and return the responsemessage that needsto be returned to MTN
        transaction.
                setServletResponseString(buildResponse(transaction).
                        getResponseAsString());
        return transaction.getServletResponseString();
    }

    /**
     * this method will build an instance of an IResponse based on the values
     * that are in the transaction object.This method defines the default
     * webservice response as used by the AV microservice architecture. This
     * method needs to be overridden if the webservice response is not the
     * default response.
     *
     * @param transaction a broker object that has been returned from the
     * controller class
     * @return the IResponse instance that you would like to return to MTN
     */
    public IResponse buildResponse(T transaction) {
        DefaultResponseBodyDetail detail = new DefaultResponseBodyDetail();
        DefaultResponseBody body = new DefaultResponseBody();
        // Build default response object
        body.setSuccess(transaction.isHttpRequestSuccessful());
        detail.setMessage(transaction.getResponse());
        detail.setResponseCode(transaction.getResponseCode().getStrVal());
        body.setDetail(detail);
        return body;
    }

    /**
     * this method writes the response String to the writer
     *
     * @param responseString the response String that needs to be sent out to
     * the writer
     * @param response the servlet response that will need to be written out
     * @throws java.io.IOException if unable to write out this needs to be
     * thrown
     */
    public void writeOut(String responseString, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println(responseString);
        out.flush();
    }
}
