/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tocchae.commons.webservice.validate;

import com.tocchae.commons.webservice.interfaces.IRequest;
import com.tocchae.commons.webservice.utils.SerializerUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class is responsilbe for parsing the incoming request string to a {@link IRequest} instance
 * @author brian
 * @param <Q> request object
 */
public class ValidateJSON<Q extends IRequest> extends Validate<Q> {

    private String timestamp;
    private static final String DATESTRINGFORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * this is a Default constuctor when you dont need to use a datasource
     */
    public ValidateJSON() {
        super();
    }

    /**
     * this method is responsible for validating the incoming request to ensure that it meets the
     * requirements
     *
     * @param envelopeClass this is the class to which the request String should be parsed to.
     * @param requestString this is the request json string
     * @return true if the validation is sucessful
     */
    @Override
    public boolean validate(Class<Q> envelopeClass, String requestString) {        
              setRequestMessage((Q) SerializerUtil.deSerialiseFromJsonString(requestString, envelopeClass));
        if (null == getRequestMessage()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATESTRINGFORMAT);
            timestamp = dateFormat.format(new Date());
            setValid(false);
            setMessage("JSON request formatted incorrectly.");
        }
        return isValid();
        
        
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }
}

