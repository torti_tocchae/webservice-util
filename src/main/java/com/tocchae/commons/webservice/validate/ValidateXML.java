/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tocchae.commons.webservice.validate;

import com.tocchae.commons.webservice.interfaces.IRequest;
import com.tocchae.commons.webservice.utils.SerializerUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author brian
 * @param <Q> request object
 */
public class ValidateXML<Q extends IRequest> extends Validate<Q> {

    private String timestamp;
    private static final String DATESTRINGFORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * this is a Default constuctor when you dont need to use a datasource
     */
    public ValidateXML() {
        super();
    }

    /**
     * this method is responsible for validating the incoming XML request to
     * ensure that it meets the requirements
     *
     * @param envelopeClass the class of the SAOP request envelope
     * @param requestString this is the request XML string
     * @return true if the validation is successful
     */
    @Override
    public boolean validate(Class<Q> envelopeClass, String requestString) {

        setRequestMessage((Q) SerializerUtil.deSerialiseFromXmlString(requestString, envelopeClass));
        if (null == getRequestMessage()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATESTRINGFORMAT);
            timestamp = dateFormat.format(new Date());
            setValid(false);
            setMessage("XML request formatted incorrectly.");
        }
        return isValid();
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }
}
