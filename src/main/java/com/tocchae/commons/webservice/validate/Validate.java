/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tocchae.commons.webservice.validate;

import com.tocchae.commons.webservice.interfaces.IRequest;

/**
 * This is the base class that is used to validate an incoming object. it
 * contains all the common attributes used by every validation
 *
 * @author Torti Ama-Njoku
 * @param <Q> request object
 */
public abstract class Validate<Q extends IRequest> implements Validation<Q> {

    private String message;
    private boolean valid;
    private Q requestMessage;

    /**
     * this is a Default constuctor when you dont need to use a datasource
     */
    public Validate() {
        valid = true;
    }

    /**
     * @return the message
     */
    @Override
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the valid
     */
    @Override
    public boolean isValid() {
        return valid;
    }

    /**
     * @param valid the valid to set
     */
    @Override
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * @return the requestMessage
     */
    public Q getRequestMessage() {
        return requestMessage;
    }

    /**
     * @param requestMessage the requestMessage to set
     */
    public void setRequestMessage(Q requestMessage) {
        this.requestMessage = requestMessage;
    }


}

