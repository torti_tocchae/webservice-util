/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.abstracts;

import java.io.IOException;
import org.apache.log4j.Logger;
import com.tocchae.commons.webservice.entities.WebserviceTransaction;
import com.tocchae.commons.webservice.enums.ResponseCodeEnum;
import com.tocchae.commons.webservice.interfaces.IRecord;
import com.tocchae.commons.webservice.interfaces.IResponse;
import com.tocchae.commons.webservice.interfaces.IRequest;

/**
 * @author Torti Ama-Njoku @ Tocchae
 * @param <R> response object
 */
public abstract class AbstractSimpleHTTPAPICall<R extends IResponse>
        extends AbstractBaseAPICall<R, IRequest> {

    String requestString;
    String responseString;
    private static final String NO_RESPONSE = "No response";
    private final boolean webserviceLoggingEnabled;

    /**
     * Default constructor
     *
     * @param transactionEntity transaction entity associated with this API call
     */
    public AbstractSimpleHTTPAPICall(IRecord transactionEntity, boolean webserviceLoggingEnabled) {
        super(transactionEntity);
        this.webserviceLoggingEnabled = webserviceLoggingEnabled;
    }

    /**
     * Build your request string to be used in the HTTP request.
     *
     * @return Q the request object
     */
    public abstract String getRequestString();

    /**
     * This method makes a webservice call to the third-party endpoint and
     * returns the JSON response object.
     *
     * @param logger for logging info and errors
     * @param responseClass class type of the response
     * @return response object of type {@code <R>}
     */
    public IResponse callEndpoint(Logger logger, Class<R> responseClass) {
        // Call the method to execute the HTTP request and return the response object
        return executeHttpRequest(logger, responseClass);
    }

    /**
     * This utility method executes the HTTP request call to a third-party
     *
     * @param logger for logging info and errors
     * @param responseClass class type of the response
     * database
     * @return the JSON response from the request
     */
    public IResponse executeHttpRequest(Logger logger, Class<R> responseClass) {

        // Execute the request and get the response
        responseString = NO_RESPONSE; //initialize the responseString variable
        requestString = getRequestString();

        // Log request
        if (webserviceLoggingEnabled) {
            logger.debug("HTTP REQUEST: " + requestString);
        }

        try {
            responseString = executeSimpleHTTPRequest();

            // Log response
            if (webserviceLoggingEnabled) {
                logger.debug("HTTP RESPONSE: " + responseString);
            }

            if (responseString != null) {
                responseBody = getResponseBody(responseString);

                responseCode = 200;
                ((WebserviceTransaction) getTransactionEntity())
                        .setHttpResponseSuccess(true);
            }

        } catch (Exception ex) {
            setExceptionResponse(ex, ex.getClass().getName(), logger);
        }

        // Set HTTP attributes in the transaction entity
        setHTTPAttributes();

        return responseBody;
    }

    /**
     * This method will implement the execution of the simple HTTP method as
     * desired by the implementer.
     *
     * @return the simple HTTP response string
     * @throws java.io.IOException when the crap hits the fan this will be
     * thrown like when you cant connect to the endpoint and stuff like that
     */
    public abstract String executeSimpleHTTPRequest() throws IOException;

    /**
     * This method builds an exception response based on the exception
     *
     * @param ex the exception object that has been thrown
     * @param errorName name of this exception
     * @param logger for logging info and errors
     */
    private void setExceptionResponse(Exception ex, String errorName, Logger logger) {
        ((WebserviceTransaction) getTransactionEntity())
                .setResponse(errorName + ": " + ex.getMessage());
        ((WebserviceTransaction) getTransactionEntity())
                .setResponseCode(ResponseCodeEnum.RESPONSE_FAILED);
        ((WebserviceTransaction) getTransactionEntity())
                .setHttpResponseSuccess(false);
        logger.fatal(null, ex);
    }

    /**
     * Set the HTTP attributes of the transaction object
     */
    protected void setHTTPAttributes() {
        ((WebserviceTransaction) getTransactionEntity())
                .setGatewayRequestString(requestString);
        ((WebserviceTransaction) getTransactionEntity())
                .setGatewayResponseString(responseString);
        ((WebserviceTransaction) getTransactionEntity())
                .setHttpResponseCode(responseCode);

    }

    /**
     * This method should be implemented by inheriting classes, specifying in
     * code the transformation of the response string into the response object.
     * This should call the appropriate method in the SerializerUtil class
     *
     * @param responseString response string
     * @return error response object
     */
    public abstract R getResponseBody(String responseString);
}
