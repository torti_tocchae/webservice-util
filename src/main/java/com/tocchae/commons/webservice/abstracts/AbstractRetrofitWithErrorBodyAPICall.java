/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.abstracts;

import org.apache.log4j.Logger;
import retrofit2.Call;
import com.tocchae.commons.webservice.entities.WebserviceTransaction;
import com.tocchae.commons.webservice.interfaces.IRecord;
import com.tocchae.commons.webservice.interfaces.IRequest;
import com.tocchae.commons.webservice.interfaces.IResponse;

/**
 * This class contains the execution steps for a building a retrofit service
 * factory, building a request, executing the request and returning the response
 * from the third-party.
 *
 * @author Torti Ama-Njoku @ Tocchae
 * @param <Q> request object
 * @param <R> successful response object
 * @param <E> error response object
 * @param <I> retrofit interface
 */
public abstract class AbstractRetrofitWithErrorBodyAPICall<Q extends IRequest, R extends IResponse, 
        E extends IResponse, I> extends AbstractRetrofitAPICall<Q, R, I> {


      
    /**
     * Default constructor
     *
     * @param transactionEntity transaction entity associated with this API call
     */
    public AbstractRetrofitWithErrorBodyAPICall(IRecord transactionEntity) {
        super(transactionEntity);
    }

    /**
     * This utility method executes the HTTP request call to a third-party
     *
     * @param logger for logging info and errors
     * @param requestCall prepared HTTP request that needs to be executed
     * @param requestBody the retrofit request body object
     * @return the JSON/XML response object from the request
     */
    @Override
    public IResponse executeHttpRequest(Logger logger, Call<R> requestCall, Q requestBody) {

        IResponse responseErrorBody = null;
        httpRequestBody = requestBody;

        // Execute the request and get the response
        try {
            response = requestCall.execute();

            if (response != null) {
                responseCode = response.code();

                ((WebserviceTransaction) getTransactionEntity())
                        .setHttpResponseSuccess(response.isSuccessful());

                // Treat successful and body and error body differently
                if (response.isSuccessful()) {
                    responseErrorBody = response.body();
                    ((WebserviceTransaction) getTransactionEntity())
                            .setGatewayResponseString(responseErrorBody.getResponseAsString());
                } else if (response.errorBody() != null) {
                    responseErrorBody = getErrorResponseBody(response.errorBody().string());
                    ((WebserviceTransaction) getTransactionEntity())
                            .setGatewayResponseString(responseErrorBody.getResponseAsString());
                }

            }
        } catch (Exception ex) {
            setExceptionResponse(ex,  ex.getClass().getName(), logger);
        }

        // Set HTTP attributes in the transaction entity
        setHTTPAttributes(httpRequestBody);

        return responseErrorBody;
    }

    /**
     * This method should be implemented by inheriting classes, specifying in
     * code the transformation of the error response string into the error
     * response object.
     *
     * @param errorResponseString error response string from the retrofit error
     * body.
     * @return error response object
     */
    public abstract E getErrorResponseBody(String errorResponseString);
}
