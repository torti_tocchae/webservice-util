/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.abstracts;

import retrofit2.Response;
import com.tocchae.commons.webservice.entities.WebserviceTransaction;
import com.tocchae.commons.webservice.interfaces.IBaseAPICall;
import com.tocchae.commons.webservice.interfaces.IRecord;
import com.tocchae.commons.webservice.interfaces.IRequest;
import com.tocchae.commons.webservice.interfaces.IResponse;

/**
 * This is the base abstract class for all third-party webservice calls
 *
 * @param <R> successful response object
 * @param <Q> request object
 * @author Torti Ama-Njoku @ Tocchae
 */
public abstract class AbstractBaseAPICall<R extends IResponse, Q extends IRequest> implements IBaseAPICall {

    Response<R> response;
    R responseBody = null;
    int responseCode = -1;

    private IRecord transactionEntity;

    /**
     * Default constructor
     *
     * @param transactionEntity transaction entity associated with this API call
     */
    public AbstractBaseAPICall(IRecord transactionEntity) {
        this.transactionEntity = transactionEntity;
    }

    @Override
    public IBaseAPICall setTransactionEntity(IRecord transactionEntity) {
        this.transactionEntity = transactionEntity;
        return this;
    }

    @Override
    public IRecord getTransactionEntity() {
        return this.transactionEntity;
    }

    /**
     * @param httpRequestBody receive the HTTP request Set the HTTP attributes
     * of the transaction object
     */
    protected void setHTTPAttributes(Q httpRequestBody) {

        ((WebserviceTransaction) getTransactionEntity())
                .setGatewayRequestString(httpRequestBody.getRequestAsString());
        ((WebserviceTransaction) getTransactionEntity())
                .setHttpResponseCode(responseCode);
    }
}
