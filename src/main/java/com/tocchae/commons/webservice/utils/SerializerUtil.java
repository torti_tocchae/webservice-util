/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.StringWriter;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

/**
 * General utility to serialise objects
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
public final class SerializerUtil {
    
    private static final Logger LOGGER = 
            Logger.getLogger(SerializerUtil.class.getName());
    
    /**
     * Default constructor
     */
    private SerializerUtil() { }
    
    /**
     * This method serialises objects into JSON/XML string for a retrofit
     * response body.
     *
     * @param objectToSerialise object to be serialised
     * @return plain xml/json string representation of the object
     */
    public static String serialiseObject(Object objectToSerialise) {
        String serialisedString = "Not Defined";
        if (objectToSerialise != null) {
            try {
                Serializer serializer = new Persister();
                StringWriter result = new StringWriter();
                serializer.write(objectToSerialise, result);
                serialisedString = result.toString();
            } catch (Exception ex) {
                LOGGER.error(null, ex);
            }
        }
        return serialisedString;
    }
    
    /**
     * This method serialises objects into JSON string for any object.
     *
     * @param objectToSerialise object to be serialised
     * @return plain json string representation of the object
     */
    public static String serialiseToJson(Object objectToSerialise) {
        String serialisedString = "Not Defined";
        if (objectToSerialise != null) {
            try {
                serialisedString = new ObjectMapper().writeValueAsString(objectToSerialise);
            } catch (JsonProcessingException ex) {
                LOGGER.error(null, ex);
            }
        }
        return serialisedString;
    }
    
    /**
     * This method deserialise a json string back into a java object using
     * jackson.
     *
     * @param jsonString json string to be deserialised into the object
     * @param objectClass object class type to deserialise the string into
     * @return deserialised object
     */
    public static Object deSerialiseFromJsonString(String jsonString, Class<?> objectClass) {
        Object deSerialisedObject = null;
        ObjectMapper mapper = new ObjectMapper();
        if (StringUtils.isNotBlank(jsonString)) {
            try {
                deSerialisedObject = mapper.readValue(jsonString, objectClass);
            } catch (IOException ex) {
                LOGGER.error(null, ex);
            }
        }
        return deSerialisedObject;
    }
    
    /**
     * This method deserialise an XML string back into a java object using simple
     * XML.
     *
     * @param xmlString xml string to be deserialised into the object
     * @param objectClass object class type to deserialise the string into
     * @return deserialised object
     */
    public static Object deSerialiseFromXmlString(String xmlString, Class<?> objectClass) {
        Object deSerialisedObject = null;
        Serializer serializer = new Persister();
        try {
            deSerialisedObject = serializer.read(objectClass, xmlString);
        } catch (Exception ex) {
            LOGGER.error(null, ex);
        }
        return deSerialisedObject;
    }
}
