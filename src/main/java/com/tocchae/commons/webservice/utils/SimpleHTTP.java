/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.utils;

import com.tocchae.commons.microservice.utils.TocchaeStringUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Simple HTTP Utility class
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
public final class SimpleHTTP {

    private static final String USER_AGENT = "Mozilla/5.0";
    
    private static final int TIMEOUT = 8000; //in milliseconds - 8 seconds

    /**
     * Constructor
     */
    private SimpleHTTP() {

    }

    /**
     * Calls an HTTP Get request with the given URL and optional header list
     *
     * @param url        URL to be called
     * @param headerList - the optional headerList to add
     * @throws java.io.IOException
     * @return HTML response
     */
    private static String sendHTTPGetEngine(String url, Map<String, String> headerList)
            throws IOException {
        // Get rid of invalid characters
        String newUrl = url.replace(" ", "%20");
        URL obj = new URL(newUrl);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        
        //set the timeout
        con.setConnectTimeout(TIMEOUT);

        //add custom headers if they were specified
        if (!headerList.isEmpty()) {
            for (Map.Entry<String, String> entry : headerList.entrySet()) {
                con.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        StringBuilder response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        //print result
        return response.toString();
    }

    /**
     * Sends a HTTP GET request to the specified URL without additional headers
     *
     * @param url - the URL to where the request will be sent to
     * @return - the response message
     * @throws IOException will throw when the crap hits the fan like if it cant connect and annoying stuff like that
     */
    public static String sendHTTPGet(String url) throws IOException {
        return sendHTTPGetEngine(url, Collections.emptyMap());
    }

    /**
     * Sends a HTTP GET request to the specified URL with additional headers
     *
     * @param url        - the URL to where the request will be sent to
     * @param headerList - the headers to include in the call
     * @return - the response message
     * @throws IOException will throw when the crap hits the fan like if it cant connect and annoying stuff like that
     */
    public static String sendHTTPGet(String url, Map<String, String> headerList) throws IOException {
        return sendHTTPGetEngine(url, headerList);
    }

    /**
     * Calls an HTTP Post request with the given URL and optional headers
     *
     * @param url           URL to be called
     * @param urlParameters HTTP Post parameters
     * @param bodyContent   Content to be sent in the Body section of the HTTP_POST
     * @param headerList    The optional header list to include in the request
     * @throws java.io.IOException
     * @throws MalformedURLException
     * @return HTML response
     */
    private static String sendHTTPPostEngine(String url, String urlParameters,
            String bodyContent, Map<String, String> headerList) throws IOException {
        // Get rid of invalid characters
        String newUrlParameters = urlParameters.replace(" ", "%20");

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        
        //set the timeout
        con.setConnectTimeout(TIMEOUT);

        // add additional custom headers if specified
        if (!headerList.isEmpty()) {
            for (Map.Entry<String, String> entry : headerList.entrySet()) {
                con.setRequestProperty(entry.getKey(), entry.getValue());
            }
        }

        // Send post request
        con.setDoOutput(true);
        // Combine URL params and BODY content
        String rebuiltUrlParameters = newUrlParameters + "\t" + bodyContent;
        try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            wr.writeBytes(rebuiltUrlParameters);
            wr.flush();
        }

        if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED
                && con.getResponseCode() != HttpURLConnection.HTTP_OK) {
            return "[HTTP Post Error: " + con.getResponseCode() + "]";
        }

        StringBuilder response;
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"))) {
            String inputLine;
            response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        return response.toString();
    }

    /**
     * Calls an HTTP Post request with the given URL and no headers
     *
     * @param url           URL to be called
     * @param urlParameters HTTP Post parameters
     * @param bodyContent   Content to be sent in the Body section of the HTTP_POST
     * @return HTTP response message
     * @throws IOException will throw when the crap hits the fan like if it cant connect and annoying stuff like that
     */
    public static String sendHTTPPost(String url, String urlParameters,
            String bodyContent) throws IOException {
        return sendHTTPPostEngine(url, urlParameters, bodyContent, Collections.emptyMap());
    }

    /**
     * Calls an HTTP Post request with the given URL with additional headers
     *
     * @param url           URL to be called
     * @param urlParameters HTTP Post parameters
     * @param bodyContent   Content to be sent in the Body section of the HTTP_POST
     * @param headerList    Header list to add to the request
     * @return HTTP response message
     * @throws IOException will throw when the crap hits the fan like if it cant connect and annoying stuff like that
     */
    public static String sendHTTPPost(String url, String urlParameters,
            String bodyContent, Map<String, String> headerList) throws IOException {
        return sendHTTPPostEngine(url, urlParameters, bodyContent, headerList);
    }

    /**
     * Parses a JSON string and returns a {@code List<String>} representation of all values paired
     * with the jsonAttribute.
     *
     * @param jsonString       String containing the complete JSON content
     * @param jsonAttribute    String referring to the JSON attribute / object name
     * @param valueSeperator   What character should be used to separate the values with?
     * @param autoNumberOffset if greater or equal to 0, each item in the 
     * {@code List<String>} will be preceded by an  auto-increment number, starting at autoNumberOffset
     * @return a {@code List<String>} of all values paired with the jsonAttribute
     */
    public static List<String> parseJSONForAttribute(String jsonString, String jsonAttribute,
            String valueSeperator, String autoNumberOffset) {
        List<String> result = new ArrayList<>();
        String numberOffset = TocchaeStringUtils.coalesce(autoNumberOffset);

        // Split the jsonString by the typical JSON structural indicators.
        StringTokenizer tokenizer = new StringTokenizer(jsonString, ",[{}]");
        while (tokenizer.hasMoreTokens()) {
            String nextToken = tokenizer.nextToken().trim();
            // At this stage, our JSON response has been split up into 
            // a list of "attribute":"value" pairs
            //
            // Now we simply extract each occurance of the key/value pair we're looking for.
            int colonPos = nextToken.indexOf(":");
            if (colonPos > 0) {
                String aKey = nextToken.substring(0, colonPos - 1).replaceAll("\"", "").trim();
                if (aKey.equalsIgnoreCase(jsonAttribute)) {
                    String aValue = nextToken.substring(colonPos + 1, nextToken.length()).replaceAll("\"", "").trim();
                    // do we add an auto-number?
                    try {
                        if (!numberOffset.isEmpty() && Integer.parseInt(numberOffset) >= 0) {
                            result.add(valueSeperator
                                    + String.valueOf(Integer.parseInt(numberOffset) + result.size())
                                    + " "
                                    + aValue);
                        } else {
                            result.add(valueSeperator + aValue);
                        }
                    } catch (NumberFormatException e) {
                        result.add(valueSeperator + aValue);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Parses a JSON string and returns a {@code List<String>} representation of all array entries
     * of a JSON Array {@code jsonArrayName}
     *
     * @param jsonString       String containing the complete JSON content
     * @param jsonArrayName    String referring to the JSON array
     * @param valueSeperator   What character should be used to separate the values with?
     * @param autoNumberOffset if greater or equal to 0, each item in the {@code List<String>} will be preceded by an
     *                         auto-increment number, starting at autoNumberOffset
     * @return a {@code List<String>} of all values paired with the jsonAttribute
     */
    public static List<String> parseJSONForArray(String jsonString, String jsonArrayName,
            String valueSeperator, String autoNumberOffset) {
        List<String> result = new ArrayList<>();
        /*
        Note: I started out by using javax.json.JsonReader, but seeing as the 
        incoming JSON response has an unknown structure, it is quite a lot of work
        navigating through the attributes, arrays, and objects to find the correct 
        key/value pair.
        
        A simpler (and faster) approach is to simply use a string tokenizer and find 
        the content of a JSON Array
        
        A Json object containing an array may look something like this:
        {@code{"statuscode":0,"statusmessage":"Success","error":[],
         "data":
            {"Dates":
                ["2016-03-23","2016-03-24","2016-03-25","2016-03-26"]
            }
        }
         */

        // Locate the Array Name
        int arrayNamePos = jsonString.indexOf(jsonArrayName);
        int arrayStartPos = jsonString.indexOf("[", arrayNamePos);
        int arrayEndPos = jsonString.indexOf("]", arrayStartPos);
        String arrayContent = jsonString.substring(arrayStartPos, arrayEndPos);

        StringTokenizer tokenizer = new StringTokenizer(arrayContent, "[,]");
        while (tokenizer.hasMoreTokens()) {

            String nextToken = tokenizer.nextToken().replaceAll("\"", "").trim();
            // do we add an auto-number?
            if (autoNumberOffset != null && Integer.parseInt(autoNumberOffset) >= 0) {
                result.add(valueSeperator
                        + String.valueOf(Integer.parseInt(autoNumberOffset) + result.size())
                        + " "
                        + nextToken);
            } else {
                result.add(valueSeperator + nextToken);
            }
        }
        return result;
    }

    /**
     * Parses a JSON string and returns a {@code String} representation of the first attribute value
     *
     * @param jsonString    String containing the complete JSON content
     * @param jsonAttribute String referring to the JSON attribute / object name
     * @return String representation of selected attribute
     */
    public static String parseJSONForAttribute(String jsonString, String jsonAttribute) {
        List<String> attributeList = parseJSONForAttribute(jsonString, jsonAttribute, "", "");
        if (attributeList == null || attributeList.isEmpty()) {
            return "";
        } else {
            return attributeList.get(0);
        }
    }

    /**
     * Returns a string without illegal JSON characters in the result
     *
     * @param jsonString JSON string with possible illegal characters
     * @return JSON response without illegal characters
     */
    public static String cleanupForJSON(String jsonString) {
        return jsonString.trim().replace("{", "").replace("}", "")
                .replace("[", "").replace("]", "").replace("(", "")
                .replace(")", "").replace("\n", "").replace("\"", "");
    }

}
