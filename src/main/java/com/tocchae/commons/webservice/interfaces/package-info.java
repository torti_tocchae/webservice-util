/**
 * The commons.interfaces package contains commons interface classes that can be
 * implemented across applications.
 * <p>
 *
 * @since 1.0
 * @see com.tocchae
 */
package com.tocchae.commons.webservice.interfaces;

