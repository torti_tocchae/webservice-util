/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.interfaces;

/**
 * This interface defines the basics of an element for the request that is used to a
 * third-party service call. Every third-party request entity should implement
 * this interface.
 *
 * @author Torti Ama-Njoku @ Tocchae
 */
@FunctionalInterface
public interface IRequest { 
    
    /**
     * This method when implemented should serialise the object using
     * AVSerialiser and return the string representation of the request object.
     * @return string representation of the request object.
     */
    String getRequestAsString();
}

