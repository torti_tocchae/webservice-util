/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.enums;

import org.apache.commons.lang3.StringUtils;

/**
* Enumeration for the status of web service calls/results
*/
public enum ResponseCodeEnum {

   /**
    * Successful status
    */
   RESPONSE_SUCCESS("01"),
   /**
    * Pending Status
    */
   RESPONSE_PENDING("02"),
   /**
    * Validation Error Status
    */
    RESPONSE_VALIDATION_ERROR("03"),
    /**
    * Retry Status
    */
    RESPONSE_RETRY("04"),
    /**
    * Failed Status
    */
   RESPONSE_FAILED("00");
   
   private final String strVal;
   /**
    * Constructor
    *
    * @param sVal String value
    */
   ResponseCodeEnum(String sVal) {
       strVal = sVal;
   }

   /**
    * @return String value of status
    */
   public String getStrVal() {
       return strVal;
   }

   /**
    * Returns the Enum corresponding to the given text representation
    *
    * @param sVal text value of enumerator
    * @return Enum value of the enumerator
    */
   public static ResponseCodeEnum getEnumValue(String sVal) {
       if (StringUtils.trim(sVal).compareToIgnoreCase(ResponseCodeEnum.RESPONSE_PENDING.toString()) == 0) {
           return ResponseCodeEnum.RESPONSE_PENDING;
       }
       if (StringUtils.trim(sVal).compareToIgnoreCase(ResponseCodeEnum.RESPONSE_SUCCESS.toString()) == 0) {
           return ResponseCodeEnum.RESPONSE_SUCCESS;
       }
       if (StringUtils.trim(sVal).compareToIgnoreCase(ResponseCodeEnum.RESPONSE_VALIDATION_ERROR.toString()) == 0) {
           return ResponseCodeEnum.RESPONSE_VALIDATION_ERROR;
       }
       if (StringUtils.trim(sVal).compareToIgnoreCase(ResponseCodeEnum.RESPONSE_RETRY.toString()) == 0) {
           return ResponseCodeEnum.RESPONSE_RETRY;
       }
       return ResponseCodeEnum.RESPONSE_FAILED;
   }
   
   /**
    *
    * @return String representation of this enumeration
    */
   @Override
   public String toString() {
       return strVal;
   }
}
