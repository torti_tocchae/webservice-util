/**
 * The commons.enums package contains commons enumerator classes.
 * <p>
 *
 * @since 1.0
 * @see com.tocchae
 */
package com.tocchae.commons.webservice.enums;

