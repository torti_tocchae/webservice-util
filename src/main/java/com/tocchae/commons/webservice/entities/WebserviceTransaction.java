/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.entities;

import com.tocchae.commons.webservice.response.DefaultResponseBody;
import com.tocchae.commons.webservice.response.DefaultResponseBodyDetail;

/**
 * This is the class that will be used to persist gateway transactions to the
 * log table/file.
 *
 * @author Torti Ama-Njoku @ Tocchae
 * @param <T> MSISDN object type
 */
public class WebserviceTransaction<T> extends Transaction<T> {

    private String servletRequestString;
    private String servletResponseString;
    private String gatewayRequestString;
    private String gatewayResponseString;
    private int httpResponseCode;
    private String identifier;
    private String executionStep;
    private String hostingOperator;
    private String endpointName;
    private boolean httpResponseSuccess;

    /**
     * Defaukt Constructor.
     */
    public WebserviceTransaction() {
        super();
    }

    /**
     * @return the servletRequestString
     */
    public String getServletRequestString() {
        return servletRequestString;
    }

    /**
     * @param servletRequestString the servletRequestString to set
     */
    public void setServletRequestString(String servletRequestString) {
        this.servletRequestString = servletRequestString;
    }

    /**
     * @return the servletResponseString
     */
    public String getServletResponseString() {
        return servletResponseString;
    }

    /**
     * @param servletResponseString the servletResponseString to set
     */
    public void setServletResponseString(String servletResponseString) {
        this.servletResponseString = servletResponseString;
    }

    /**
     * @return the gatewayRequestString
     */
    public String getGatewayRequestString() {
        return gatewayRequestString;
    }

    /**
     * @param gatewayRequestString the gatewayRequestString to set
     */
    public void setGatewayRequestString(String gatewayRequestString) {
        this.gatewayRequestString = gatewayRequestString;
    }

    /**
     * @return the gatewayResponseString
     */
    public String getGatewayResponseString() {
        return gatewayResponseString;
    }

    /**
     * @param gatewayResponseString the gatewayResponseString to set
     */
    public void setGatewayResponseString(String gatewayResponseString) {
        this.gatewayResponseString = gatewayResponseString;
    }

    /**
     * Returns the HTTP response code retrieved from calling the request
     *
     * @return integer code
     */
    public int getHttpResponseCode() {
        return httpResponseCode;
    }

    /**
     * Sets the HTTP response code retrieved from calling the request
     *
     * @param httpResponseCode integer code
     * @return this instance
     */
    public WebserviceTransaction setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
        return this;
    }

    /**
     * Returns the unique identifier/key used for this record
     *
     * @return key used to uniquely identify this record
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Sets the unique identifier/key used for this record
     *
     * @param identifier the key used to uniquely identify this record
     * @return this instance
     */
    public WebserviceTransaction setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    /**
     * Returns the sequence indicator of this particular call.
     *
     * Example, "1Of4"
     *
     * @return String indication of the order and position of this record in a
     * sequence of steps.
     */
    public String getExecutionStep() {
        return executionStep;
    }

    /**
     * Defines an indicator of the order of this record/transaction in a
     * sequence of causal-dependent actions.
     *
     * @param executionStep String, i.e. "1Of4"
     * @return this instance
     */
    public WebserviceTransaction setExecutionStep(String executionStep) {
        this.executionStep = executionStep;
        return this;
    }

    /**
     * This is the name of the hostingOperator that is hosting the endpoint that
     * is being called.
     *
     * @return Operator name
     */
    public String getHostingOperator() {
        return hostingOperator;
    }

    /**
     * Sets the hostingOperator name that is hosting the endpoint that is being
     * called.
     *
     * @param hostingOperator Operator name
     * @return this instance
     */
    public WebserviceTransaction setHostingOperator(String hostingOperator) {
        this.hostingOperator = hostingOperator;
        return this;
    }

    /**
     * Returns the name of the endpoint that is being called
     *
     * @return String representing the end-point name
     */
    public String getEndpointName() {
        return endpointName;
    }

    /**
     * Sets the name of the endpoint that is being called
     *
     * @param endpointName String representing the end-point name
     * @return this instance
     */
    public WebserviceTransaction setEndpointName(String endpointName) {
        this.endpointName = endpointName;
        return this;
    }

    /**
     * To determine if the HTTP request was successful or not
     *
     * @return the httpResponseSuccess
     */
    public boolean isHttpRequestSuccessful() {
        return httpResponseSuccess;
    }

    /**
     *
     * @param httpResponseSuccess the httpResponseSuccess to set
     * @return this instance
     */
    public WebserviceTransaction setHttpResponseSuccess(boolean httpResponseSuccess) {
        this.httpResponseSuccess = httpResponseSuccess;
        return this;
    }

    /**
     * This method defines the default webservice response as used by the AV
     * microservice architecture. The response is serialised into a json string
     * by default. This method needs to be overridden if the webservice response
     * is not the default response.
     *
     * @return returns the webservice response to be sent to the calling party
     */
    public String getWebserviceResponse() {
        DefaultResponseBodyDetail detail = new DefaultResponseBodyDetail();
        DefaultResponseBody body = new DefaultResponseBody();

        // Build default response object
        body.setSuccess(isHttpRequestSuccessful());
        detail.setMessage(getResponse());
        detail.setResponseCode(getResponseCode().getStrVal());
        body.setDetail(detail);
        return body.getResponseAsString();
    }
}
