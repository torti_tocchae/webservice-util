/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be 
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.entities;

import java.math.BigDecimal;
import java.util.Date;
import com.tocchae.commons.webservice.enums.ResponseCodeEnum;
import com.tocchae.commons.webservice.interfaces.IRecord;

/**
 * This abstract class provides the attributes of a standard transaction. A class
 * that inherits from this class will need to define how to create a new transaction
 * how to update a transaction and how to retrieve a transaction
 *
 * @author Torti Ama-Njoku @ Tocchae
 * @param <T> MSISDN object type
 */
public abstract class Transaction<T> implements IRecord {

    private Long transactionId;
    private String operationId;
    private T msisdn;
    private BigDecimal value;
    private Date timestamp;
    private String response;
    private ResponseCodeEnum responseCode;
    private String clientTransactionId;
    private String operatorTransactionId;
    private int attempts;

    /**
     * Default Constructor.
     */
    public Transaction() { }

    /**
     * @return the avTransactionId
     */
    @Override
    public Long getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     * @return this instance
     */
    @Override
    public Transaction setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    /**
     * @return the operationId
     */
    @Override
    public String getOperationId() {
        return operationId;
    }

    /**
     * @param operationId the operationId to set
     * @return this instance
     */
    @Override
    public Transaction setOperationId(String operationId) {
        this.operationId = operationId;
        return this;
    }

    /**
     * @return the msisdn
     */
    public T getMsisdn() {
        return msisdn;
    }

    /**
     * @param msisdn the msisdn to set
     * @return this instance
     */
    public Transaction setMsisdn(T msisdn) {
        this.msisdn = msisdn;
        return this;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value the value to set
     * @return this instance
     */
    public Transaction setValue(BigDecimal value) {
        this.value = value;
        return this;
    }

    /**
     * @return the timestamp
     */
    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     * @return this instance
     */
    @Override
    public Transaction setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    /**
     * @return the response
     */
    @Override
    public String getResponse() {
        return response;
    }

    /**
     * @param response the response to set
     * @return this instance
     */
    @Override
    public Transaction setResponse(String response) {
        this.response = response;
        return this;
    }

    /**
     * @return the responseCode
     */
    @Override
    public ResponseCodeEnum getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     * @return this instance
     */
    @Override
    public Transaction setResponseCode(ResponseCodeEnum responseCode) {
        this.responseCode = responseCode;
        return this;
    }

    /**
     * @return the clientTransactionId
     */
    public String getClientTransactionId() {
        return clientTransactionId;
    }

    /**
     * @param clientTransactionId the clientTransactionId to set
     * @return this instance
     */
    public Transaction setClientTransactionId(String clientTransactionId) {
        this.clientTransactionId = clientTransactionId;
       return this;
     }

    /**
     * @return the operatorTransactionId
     */
    public String getOperatorTransactionId() {
        return operatorTransactionId;
    }

    /**
     * @param operatorTransactionId the operatorTransactionId to set
     * @return this instance
     */
    public Transaction setOperatorTransactionId(String operatorTransactionId) {
        this.operatorTransactionId = operatorTransactionId;
        return this;
    }

    /**
     * @return the attempts
     */
    @Override
    public int getAttempts() {
        return attempts;
    }

    /**
     * @param attempts the attempts to set
     * @return this instance
     */
    @Override
    public Transaction setAttempts(int attempts) {
        this.attempts = attempts;
        return this;
    }
}

