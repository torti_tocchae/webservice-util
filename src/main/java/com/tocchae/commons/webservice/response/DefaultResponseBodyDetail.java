/*
 * Unless differently specified, all code and IP in this
 * project is the property of Tocchae.  No code may be
 * used or copied under any circumstances.
 */
package com.tocchae.commons.webservice.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Torti Ama-Njoku
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefaultResponseBodyDetail {
    
    private String message;
    private String responseCode;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
