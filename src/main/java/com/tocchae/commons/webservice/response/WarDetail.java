package com.tocchae.commons.webservice.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * the pojo used to build the War detail
 * @author Torti Ama-Njoku @ Tocchae
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WarDetail {

    @JsonProperty("warVersion")
    private String warVersion;
    @JsonProperty("name")
    private String name;
    @JsonProperty("buildDate")
    private String buildDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties;

    /**
     * Default Constructor
     */
    public WarDetail() {
        additionalProperties = new HashMap<>();
    }

    /**
     * the build number of this particular deployment
     *
     * @return the build number of this particular deployment
     */
    @JsonProperty("warVersion")
    public String getWarVersion() {
        return warVersion;
    }

    /**
     * set the build version of this particular deployment
     *
     * @param warVersion the build version of this particular deployment
     * @return this object
     */
    @JsonProperty("warVersion")
    public WarDetail setWarVersion(String warVersion) {
        this.warVersion = warVersion;
        return this;
    }

    /**
     * the name of the war file that has been deployed
     *
     * @return the name of the war file that has been deployed
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * set the name of the war file that has been deployed
     *
     * @param name the name of the war file that has been deployed
     * @return this object
     */
    @JsonProperty("name")
    public WarDetail setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * the date when this war was built
     *
     * @return the date when this war was built
     */
    @JsonProperty("buildDate")
    public String getBuildDate() {
        return buildDate;
    }

    /**
     * set the date when this war was built
     *
     * @param buildDate the date when this war was built
     * @return this Object
     */
    @JsonProperty("buildDate")
    public WarDetail setBuildDate(String buildDate) {
        this.buildDate = buildDate;
        return this;
    }

    /**
     * return any additional parameters
     *
     * @return any addtional parameters
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * set any addtional parameters
     *
     * @param name the name of the parameter
     * @param value the value of said parameter
     * @return this object
     */
    @JsonAnySetter
    public WarDetail setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
