package com.tocchae.commons.webservice.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tocchae.commons.webservice.interfaces.IResponse;
import com.tocchae.commons.webservice.utils.SerializerUtil;

/**
 * the pojo that is used to build the JSON response message
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetStatusResponse implements IResponse {

    @JsonProperty("warDetail")
    private WarDetail warDetail;
    @JsonProperty("endpoints")
    private List<Endpoint> endpoints;
    @JsonIgnore
    private Map<String, Object> additionalProperties;

    /**
     * Default Constructor
     */
    public GetStatusResponse() {
        endpoints = null;
        additionalProperties = new HashMap<>();
    }

    /**
     * the warDetail    
     * @return the war detail
     */
    @JsonProperty("warDetail")
    public WarDetail getWarDetail() {
        return warDetail;
    }

    /**
     * set the warDetail 
     * @param warDetail object
     * @return this object
     */
    @JsonProperty("warDetail")
    public GetStatusResponse setWarDetail(WarDetail warDetail) {
        this.warDetail = warDetail;
        return this;
    }

    /**
     * return a list of endpoints
     * @return return a list of endpoints
     */
    @JsonProperty("endpoints")
    public List<Endpoint> getEndpoints() {
        return endpoints;
    }

    /**
     * set a list of endpoints
     * @param endpoints the list of endpoints
     * @return this object
     */
    @JsonProperty("endpoints")
    public GetStatusResponse setEndpoints(List<Endpoint> endpoints) {
        this.endpoints = endpoints;
        return this;
    }

    /**
     * return any additional parameters
     * @return any addtional parameters
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * set any addtional parameters
     * @param name the name of the parameter
     * @param value the value of said parameter
     * @return this object
     */
    @JsonAnySetter
    public GetStatusResponse setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    @JsonIgnore
    public String getResponseAsString() {
        return SerializerUtil.serialiseToJson(this);
    }

}
